export default {
    name: "Pagination",
    props: ['users', 'currentPage', 'pageSize'],
    methods: {
        updatePage(pageNumber) {
            this.$emit("pageupdate", pageNumber);
          },
        totalPages() {
            return Math.ceil(this.users.length / this.pageSize);
          },
        showPreviousLink() {
            return this.currentPage == 0 ? false : true;
          },
        showNextLink() {
            return this.currentPage == (this.totalPages() - 1) ? false : true;
        }  
    }
}