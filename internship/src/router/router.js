import {createWebHistory, createRouter} from 'vue-router'
import Landing from '../components/Landing'
import userDetails from '../components/userDetails'
import Add from '../components/Add'

const history = createWebHistory()
const router = createRouter({
    history,
    routes: [{
        path: '/',
        component: Landing
    },
    {
        path: '/:userId',
        component: userDetails

    },
    {
        path: '/add',
        component: Add
    }]
})

export default router